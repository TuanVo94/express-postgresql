const { UnauthorizedException } = require("../core/error.response");

const authentication = async (req, res, next) => {
  if (!req.headers.authorization) {
    throw new UnauthorizedException()
  }
  next();
};

module.exports = { authentication };
