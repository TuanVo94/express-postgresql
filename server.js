const { PORT } = require("./configs");
const app = require("./src/app");

const server = app.listen(PORT, () => {
  console.log(`server started: http://localhost:${PORT}`);
});

process.on("SIGINT", () => {
  server.close(() => {
    console.log("Exit express");
    process.exit();
  });
});