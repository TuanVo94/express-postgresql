const { BadRequestException } = require("../core/error.response");

const validateBody = (schema) => (req, res, next) => {
  const validatorResult = schema.validate(req.body);
  if (validatorResult.error) {
    throw new BadRequestException(validatorResult.error.details[0].message);
  }
  req.body = validatorResult.value;
  return next();
};

const validateParam = (schema, name) => (req, res, next) => {
  const validatorResult = schema.validate(req.params[name]);

  if (validatorResult.error) {
    throw new BadRequestException(validatorResult.error.details[0].message);
  }
  return next();
};

const validateQuery = (schema) => (req, res, next) => {
  const validatorResult = schema.validate(req.query);

  if (validatorResult.error) {
    throw new BadRequestException(validatorResult.error.details[0].message);
  }

  req.query = validatorResult.value;
  return next();
};

module.exports = {
  validateBody,
  validateParam,
  validateQuery,
};
