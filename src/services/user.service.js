const { findAllUserPaging, newNormalUser } = require("../models/repositories/user.repository");

class UserService {
  static getUsers = async (page, limit) => {
    return {
      page,
      limit,
      data: await findAllUserPaging({}, (page - 1) * limit, limit),
    };
  };

  static createUser = async (userInfo) => {
    return await newNormalUser(userInfo);
  };
}

module.exports = UserService;
