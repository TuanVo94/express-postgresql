const { DB_LOG } = require("../../configs");
const PostgresDataSource = require("./postgres.db");

class Database {
  constructor(type = "postgres") {
    this.connect(type);
  }

  connect(type = "postgres") {
    if (type === "postgres") {
      if (DB_LOG) {
        PostgresDataSource.setOptions({
          logging: true,
          logger: "advanced-console",
        });
      }
      PostgresDataSource.initialize()
        .then(() => {
          console.log("Data Source has been initialized!");
        })
        .catch((err) => {
          console.error("Error during Data Source initialization", err);
        });
    }
  }

  static getInstance() {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

const instancePostgres = Database.getInstance();

module.exports = instancePostgres;
