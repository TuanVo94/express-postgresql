const userController = require("../controllers/user.controller");
const { authentication } = require("../midlewares/authentication");
const { validateQuery, validateBody } = require("../midlewares/requestValidator");
const { pagingQuerySchema } = require("../validators/paging.validator");
const { newUserSchema } = require("../validators/user.validator");

const router = require("express-promise-router")();

router.use(authentication);
router.get("/", validateQuery(pagingQuerySchema), userController.getUsers);
router.post("/", validateBody(newUserSchema), userController.createUser);

module.exports = router;
