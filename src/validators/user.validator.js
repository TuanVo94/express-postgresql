const Joi = require("@hapi/joi");

const newUserSchema = Joi.object().keys({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  age: Joi.number().positive().integer().required(),
});

module.exports = {
  newUserSchema,
};
