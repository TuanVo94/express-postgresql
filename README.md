setup lint
https://dev.to/devland/set-up-a-nodejs-app-with-eslint-and-prettier-4i7p

pre-commit husky
https://duncanlew.medium.com/getting-started-with-husky-and-lint-staged-for-pre-commit-hooks-c2764d8c9ae

# run source
- cp .env.example .env
- npm install
- npm run local

# auto gen migration file
- npm run migration:make

# create a invidual migration file
- npm run migration:create --name=$name

# apply migration
- npm run migration:run