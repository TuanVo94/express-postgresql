const { OK, Created } = require("../core/success.response");
const UserService = require("../services/user.service");

class UserController {
  getUsers = async (req, res, next) => {
    return new OK({
      message: "Get list user success",
      metadata: await UserService.getUsers(req.query.page, req.query.limit),
    }).send(res);
  };

  createUser = async (req, res, next) => {
    return new Created({
      message: "User created",
      metadata: await UserService.createUser(req.body),
    }).send(res);
  };
}

module.exports = new UserController();
