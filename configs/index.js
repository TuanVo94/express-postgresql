require("dotenv").config();

const PORT = process.env.PORT || 3001;
const DB_LOG = process.env.DB_LOG || false;
module.exports = {
  PORT,
  DB_LOG,
};
