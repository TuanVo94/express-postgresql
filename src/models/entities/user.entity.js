const { EntitySchema } = require("typeorm");

const UserEntity = new EntitySchema({
  name: "Users",
  columns: {
    id: {
      primary: true,
      type: "int",
      generated: true,
    },
    name: {
      type: "varchar",
    },
    role: {
      type: "varchar",
    },
    email: {
      type: "varchar",
    },
    age: {
      type: "int",
    },
    is_active: {
      type: "boolean",
      default: true,
    },
  },
});

module.exports = UserEntity;