const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class Migration1707796570742 {
    name = 'Migration1707796570742'

    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "users" ALTER COLUMN "is_active" SET DEFAULT true`);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "users" ALTER COLUMN "is_active" DROP DEFAULT`);
    }
}
