const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class Migration1707726971788 {
    name = 'Migration1707726971788'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "role" character varying NOT NULL, "email" character varying NOT NULL, "age" integer NOT NULL, "is_active" boolean NOT NULL, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
    }

    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "users"`);
    }
}
