const Joi = require('@hapi/joi');

const pagingQuerySchema = Joi.object().keys({
  page: Joi.number().min(1).max(5000).default(1),
  limit: Joi.number().min(1).max(1000).default(10),
  search: Joi.string().allow(''),
});

module.exports = {
  pagingQuerySchema,
};

