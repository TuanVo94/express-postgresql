const { DataSource } = require("typeorm");

const PostgresDataSource = new DataSource({
  type: "postgres",
  host: process.env.DB_HOST || "localhost",
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME || "postgres",
  password: process.env.DB_PASSWORD || "password",
  database: process.env.DB_NAME || "postgres",
  synchronize: false,
  entities: ["./src/models/entities/*.entity.js"],
  migrations: [
    "./src/models/migrations/*.js", // path to your migration files
  ],
  cli: {
    entitiesDir: "./src/models/entities",
    migrationsDir: "./src/models/migrations",
  },
});

module.exports = PostgresDataSource;
