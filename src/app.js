const express = require("express");
const { default: helmet } = require("helmet");
const compression = require("compression");

const app = express();

app.use(helmet()); // protect scan header from client
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// init DB
require("./dbs");

app.get("/", (req, res, next) => {
  return res.status(200).json({
    message: "Welcome home 2",
  });
});

app.get("/ping", (req, res, next) => {
  return res.status(200).json({
    message: "pong",
  });
});

app.use("/v1", require("./routers"));

app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  const statusCode = error.status || 500;
  return res.status(statusCode).json({
    status: "error",
    message: error.message,
    // stack: error.stack,
    code: statusCode,
  });
});

module.exports = app;
