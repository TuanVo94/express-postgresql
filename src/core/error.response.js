class BaseExeption extends Error {
  constructor(message, status) {
    super(message);
    this.status = status;
  }
}

class BadRequestException extends BaseExeption {
  constructor(message = "Bad Request", status = 400) {
    super(message, status);
  }
}

class ForbidenException extends BaseExeption {
  constructor(message = "Forbiden", status = 403) {
    super(message, status);
  }
}

class UnauthorizedException extends BaseExeption {
  constructor(message = "Unauthorized", status = 401) {
    super(message, status);
  }
}

module.exports = {
  ForbidenException,
  UnauthorizedException,
  BadRequestException,
};
