const router = require("express-promise-router")();

router.use("/users", require("./user.router"));

module.exports = router;