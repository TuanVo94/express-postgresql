const PostgresDataSource = require("../../dbs/postgres.db");
const UserEntity = require("../entities/user.entity");

const userRepository = PostgresDataSource.getRepository(UserEntity);

const findAllUserPaging = async (condition = {}, skip, limit) => {
  return await userRepository.find({ where: condition, skip, take: limit });
};

const newNormalUser = async (data) => {
  data.role = "user";
  const user = await userRepository.save(data);
  return user;
};

module.exports = {
  userRepository,
  findAllUserPaging,
  newNormalUser,
};
